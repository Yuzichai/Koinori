import fs from 'node:fs';
import path from 'node:path';

const modulesDir = './plugins/Koinori/modules';
const dirNames = fs.readdirSync(modulesDir).filter(file => fs.statSync(path.join(modulesDir, file)).isDirectory());

let apps = {};

const loadModuleInitFile = async (moduleName) => {
    const initFilePath = path.join(modulesDir, moduleName, '_init_.js');

    try {
        const module = await import(initFilePath);
        apps[moduleName] = module[Object.keys(module)[0]];
        console.log(`${moduleName} loader...`);
    } catch (error) {
        logger.error(`载入插件错误：${logger.red(moduleName)}`);
        logger.error(error);
    }
};

const loadModules = async () => {
    const loadPromises = dirNames.map(loadModuleInitFile);
    await Promise.allSettled(loadPromises);
};

await loadModules();

export { apps };
