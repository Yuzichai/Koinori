let segment
try {
    segment = (await import("icqq")).segment
} catch (err) {
    segment = (await import("oicq")).segment
}

async function send(e, message, image) {
    if (image) {
        return e.reply([message, segment.image(image)])
    }
    return e.reply(message)
}

export default { send }