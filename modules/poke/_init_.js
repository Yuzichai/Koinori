import plugin from '../../../lib/plugins/plugin.js'
import cfg from '../../../lib/config/config.js'
import common from '../../../lib/common/common.js'
import seesion from '../_interact.js'
import random from '../random.js'
import money from '../money.js'
import R from '../_R.js'

let segment
try {
    segment = (await import("icqq")).segment
} catch (err) {
    segment = (await import("oicq")).segment
}

let CD = false
const POKE_GET = 0.9 // 不回戳的概率
let picProbability = 75 // 触发图片概率
const _path = process.cwd().replace(/\\/g, "/");
let face_o = ['呆_白.jpg', '呆_黑.jpg', '呆_混合.jpg', '呆_黑_2.jpg', '呆_混合_R.jpg']  // O组表情
let face_a = ['不可以A_白.jpg', '不可以A_黑.jpg', '不可以A_混合.jpg', '不可以A_混合_R.jpg']  // A组表情
let face_b = ['不可以B_白.jpg', '不可以B_黑.jpg', '不可以B_混合.jpg', '不可以B_混合_R.jpg']  // B组表情
let feed_back = ['不可以戳戳>_<', '要戳坏掉了>_<']  // 文字表情
let poke_back = ['戳_白.jpg', '戳_混合.jpg', '戳_混合_bb.jpg', '戳_混合_wb.jpg', '戳_混合_ww.jpg',
    '戳_黑.jpg', '戳_混合_R.jpg', '戳_混合_Rbb.jpg', '戳_混合_Rwb.jpg', '戳_混合_Rww.jpg',
    '戳_黑.jpg', '戳_白.jpg', '戳_白.jpg', '戳_黑.jpg', '戳_白.jpg', '戳_黑.jpg']  // 反戳表情
let good_back = ['欸嘿嘿_白.jpg', '欸嘿嘿_混合.jpg', '欸嘿嘿_混合_bb.jpg', '欸嘿嘿_混合_wb.jpg', '欸嘿嘿_混合_ww.jpg',
    '欸嘿嘿_黑.jpg', '欸嘿嘿_混合_R.jpg', '欸嘿嘿_混合_Rbb.jpg', '欸嘿嘿_混合_Rwb.jpg', '欸嘿嘿_混合_Rww.jpg',
    '欸嘿嘿_白.jpg', '欸嘿嘿_黑.jpg', '欸嘿嘿_白.jpg', '欸嘿嘿_黑.jpg', '欸嘿嘿_白.jpg', '欸嘿嘿_黑.jpg']  // 欸嘿嘿表情
let face_ugo = ['戳_动图.gif']

export class poke extends plugin {
    constructor() {
        super({
            name: '戳戳反应',
            dsc: '戳戳冰祈，冰祈会有回应！如果冰祈发出了“nya~”，则说明获得了25颗星星 如果冰祈发出了“欸嘿嘿”，则说明获得了10枚金币',
            event: 'notice.group.poke',
            priority: -1,
            rule: [
                {
                    reg: '.*',
                    fnc: 'poke_back_function'
                }
            ]
        })
    }

    async poke_back_function(e) {
        let uid = e.target_id
        if (CD) {
            return true
        } else {
            CD = true
            setTimeout(function () {
                CD = false
            }, 2000)
            if (e.target_id === cfg.qq) {
                let a = random.random()
                if (a > POKE_GET) {
                    let poke_back_img = random.choice(poke_back)
                    await seesion.send(e, '戳回去');
                    await common.sleep(500);
                    await e.group.pokeMember(e.operator_id);
                    await common.sleep(500);
                    await e.reply(segment.image(R('img', `poke_emo/${poke_back_img}`)))
                } else if (a < 0.1) {
                    money.increase_user_money(uid, "starstone", 50)
                    await seesion.send(e, 'nya~')
                } else if (a < 0.3) {
                    let face_o_img = random.choice(face_o)
                    console.log(face_o_img)
                    await e.reply(segment.image('img', `poke_emo/${face_o_img}`))
                } else if (a <= 0.496) {
                    let face_a_img = random.choice(face_a)
                    console.log(face_a_img)
                    await e.reply(segment.image('img', `poke_emo/${face_a_img}`))
                } else if (a > 0.496 && a < 0.504) {
                    let bonus_gold = random.randint(25, 100)
                    await e.reply([`戳到了一个彩蛋~获得了${bonus_gold}枚金币与两枚幸运币!`, segment.image(R('img', `emotion/震惊.png`))])
                } else if (a < 0.7) {
                    let face_b_img = random.choice(face_b)
                    console.log(face_b_img)
                    await e.reply([segment.image(R('img', `poke_emo/${face_b_img}`))])
                } else if (a > 0.7 || a < 0.9) {
                    let feed_back_choice = random.choice(feed_back)
                    console.log(feed_back_choice)
                    await e.reply(segment.image(R('img', `poke_emo/${feed_back_choice}`)))
                } else if (a >= 0.9) {
                    money.increase_user_money(uid, "gold", 10)
                    await seesion.send(e, '诶嘿嘿')
                    let goodBackImg = random.choice(good_back)
                    let image_file = R('img', `poke_emo/${goodBackImg}`)
                    await e.reply(segment.image(image_file))
                }
                else {
                    return
                }
            }
        }
    }
}
