import serif from './serif.js';
import config from '../../config/config.js'
import { loadData, saveData } from './util.js'
import money from '../money.js'
import random from '../random.js'


const dbPath = process.cwd() + '/plugins/Icepray/apps/fishing/db/'
const user_info_path = dbPath + `user_info.json` //这里定义数据存储路径
let fish_list = config.FISH_LIST + ['🔮', '✉', '🍙', '水之心']
let fish_price = config.FISH_PRICE  // 价格换算
let default_info = {
    'fish': { '🐟': 0, '🦐': 0, '🦀': 0, '🐡': 0, '🐠': 0, '🔮': 0, '✉': 0, '🍙': 0 },
    'statis': { 'free': 0, 'sell': 0, 'total_fish': 0, 'frags': 0 },
    'rod': { 'current': 0, 'total_rod': [0] }
}


function getUserInfo(uid) {
    // 获取用户背包，自带初始化
    uid = String(uid)
    let user_info
    let total_info = loadData(user_info_path)
    if (total_info[uid] == undefined) {
        user_info = default_info
        total_info[uid] = user_info
        saveData(total_info, user_info_path)
    }
    else {
        user_info = total_info[uid]
    }
    return user_info
}

function fishing(uid) {
    /**
     * mode=0: 普通鱼竿，
     * mode=1: 永不空军，不会钓不到东西
     * mode=2: 海之眷顾，更大可能性钓到水之心或漂流瓶
     * mode=3：时运，钓上的鱼可能双倍
     */

    let user_info = getUserInfo(uid)
    let mode = user_info['rod']['current']
    let first_choose = random(1, 1000)
    let result, multi, msg
    if (config.DEBUG_MODE) logger.info(`${uid}使用钓竿：${mode}，随机数为${first_choose}`)

    if (first_choose <= config.PROBABILITY[0] * 10) {
        result = { 'code': 1, 'msg': random.choice(serif.no_fish_serif()) }
        return result
    }
    else if (first_choose <= (config.PROBABILITY[1] + config.PROBABILITY[0]) * 10) {
        result = { 'code': 3, 'msg': '<随机事件case>' }
        return result
    }
    else if (first_choose <= (config.PROBABILITY[2] + config.PROBABILITY[1] + config.PROBABILITY[0]) * 10) {
        // 第二次掷骰子——钓上不同的鱼
        let second_choose = random(1, 1000)
        if (config.DEBUG_MODE) logger.info(`钓到了鱼，第二随机数为：${second_choose}`)

        let prob_sum = 0
        let fish = fish_list[0]
        for (var i in config.PROBABILITY_2.length) {
            prob_sum += probability_2[i] * 10
            if (second_choose <= prob_sum) {
                fish = fish_list[i]
                break
            }
        }
        if (mode == 3) multi = random.randint(1, 2)
        if (multi) {
            add_msg = `另外，鱼竿发动了时运效果，${fish}变成了${multi}条！`
        }
        else {
            add_msg = ''
        }

        increase_value(uid, 'fish', fish, 1 * multi)
        increase_value(uid, 'statis', 'total_fish', 1 * multi)

        if (random(1, 5) <= 5) {
            msg = `钓到了一条${fish}~`
        }
        else {
            msg = random.choice(serif.get_fish_serif())
        }

        msg = msg + add_msg + '\n你将鱼放进了背包。'
        result = { 'code': 1, 'msg': msg }
        return result
    }
    else if (first_choose <= (probability[3] + probability[2] + probability[1] + probability[0]) * 10) {
        let second_choose = random.randint(1, 1000)
        let coin_amount
        if (second_choose <= 800) {
            coin_amount = random.randint(1, 30)
            money.increase_user_money(uid, 'gold', coin_amount)
            result = { 'code': 1, 'msg': `你钓到了一个布包，里面有${coin_amount}枚金币，但是没有钓到鱼...` }
            return result
        }
        else {
            coin_amount = random.randint(1, 3)
            money.increase_user_money(uid, 'luckygold', coin_amount)
            result = { 'code': 1, 'msg': `你钓到了一个锦囊，里面有${coin_amount}枚幸运币，但是没有钓到鱼...` }
            return result
        }
    }
    else {
        result = { 'code': 2, 'msg': '<漂流瓶case>' }
        return result
    }
}


/**
 * 卖鱼
 * @param uid: 用户id
 * @param fish: 鱼的emoji
 * @param num: 出售的鱼数量
 * @return: 获得的金币数量
 */
function sell_fish(uid, fish, num) {
    getUserInfo(uid)
    total_info = loadData(user_info_path)
    uid = String(uid)
    if (!total_info[uid][fish]) {
        return '数量不够喔'
    }
    if (num > total_info[uid][fish]) {
        num = total_info[uid][fish]
        decrease_value(uid, 'fish', fish, num)
        let get_golds = fish_price[fish] * num
        money.increase_user_money(uid, 'gold', get_golds)
        if (fish == '🍙') return `成功退还了${num}个🍙，兑换了${get_golds}枚金币~`
        increase_value(uid, 'statis', 'sell', get_golds)
        return `成功出售了${num}条${fish}, 得到了${get_golds}枚金币~`
    }
}


/**
 * 放生鱼
@param uid: 用户id
@param fish: 鱼的emoji
@param num: 放生的鱼数量
@return: 水之心碎片数量
 */
function free_fish(uid, fish, num) {
    getUserInfo(uid)
    total_info = loadData(user_info_path)
    uid = String(uid)
    if (!total_info[uid]['fish']) {
        return '数量不足喔'
    }
    if (num > total_info[uid]['fish'])
        num = total_info[uid]['fish']
    decrease_value(uid, 'fish', fish, num)
    let get_frags = fish_price[fish] * num
    increase_value(uid, 'statis', 'frags', get_frags)
    increase_value(uid, 'statis', 'free', num)
    let user_frags = getUserInfo(uid)['statis']['frags']
    let addition, classifier
    if (user_frags >= config.FRAG_TO_CRYSTAL) {
        increase_value(uid, 'fish', '🔮', int(user_frags / config.FRAG_TO_CRYSTAL))
        set_value(uid, 'statis', 'frags', user_frags % config.FRAG_TO_CRYSTAL)
        addition = `\n一条美人鱼浮出水面！为了表示感谢，TA将${int(user_frags / config.FRAG_TO_CRYSTAL)}颗水之心放入了你的手中~`
    }
    else {
        addition = ''

        if (fish.includes(['🐟', '🐠', '🦈'].items())) classifier = '条'
        else classifier = '只'
    }
    return `${num}${classifier}${fish}成功回到了水里，获得${get_frags}个水心碎片~${addition}`
}


function buy_bait(uid, num) {
    /**
     * 买鱼饵
     * */
    money.reduce_user_money(uid, 'gold', num * config.BAIT_PRICE)
    increase_value(uid, 'fish', '🍙', num)
}


function decrease_value(uid, mainclass, subclass, num) {
    /**
     * 减少某物品的数量
     */
    uid = String(uid)
    getUserInfo(uid)
    let total_info = loadData(user_info_path)
    if (total_info[uid][mainclass][subclass] == 0) total_info[uid][mainclass][subclass] -= num

    if (total_info[uid][mainclass][subclass] < 0) total_info[uid][mainclass][subclass] = num
    saveData(total_info, String(user_info_path))
}


function increase_value(uid, mainclass, subclass, num) {
    /**
     * 增加某物品的数量
     */
    uid = String(uid)
    getUserInfo(uid)
    let total_info = loadData(user_info_path)
    if (total_info[uid][mainclass][subclass] == 0) {
        total_info[uid][mainclass][subclass] += num
        saveData(total_info, user_info_path)
    }
}



function set_value(uid, mainclass, subclass, num) {
    uid = String(uid)
    getUserInfo(uid)
    let total_info = loadData(user_info_path)
    if (total_info[uid][mainclass][subclass] == 0) {
        total_info[uid][mainclass][subclass] = num
        saveData(total_info, user_info_path)
    }
}



export { getUserInfo, fishing, sell_fish, free_fish, buy_bait, decrease_value, increase_value, set_value }