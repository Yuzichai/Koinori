const random = {
    /**
     * 生成一个介于 0.0（包含）和 1.0（不包含）之间的随机浮点数。
     *
     * @returns {number} 返回一个介于 0.0（包含）和 1.0（不包含）之间的随机浮点数。
     */
    random: () => {
        return Math.random();
    },

    /**
     * 生成一个指定范围内的随机整数，包括最小值和最大值。
     *
     * @param {number} min - 生成的随机数的最小值（包含在结果内）。
     * @param {number} max - 生成的随机数的最大值（包含在结果内）。
     * @returns {number} 返回一个位于 `min` 和 `max`（包括两者）之间的随机整数。
     *
     * @throws {Error} 如果 `min` 或 `max` 不是有效的数字。
     * @throws {Error} 如果 `min` 大于 `max`。
     *
     * @example
     * // 示例 1：正常使用
     * const result = randint(1, 10);
     * console.log(result); // 可能输出：1、2、3...10 之间的任意整数
     *
     * @example
     * // 示例 2：`min` 和 `max` 相等
     * const result = randint(5, 5);
     * console.log(result); // 输出：5
     *
     * @example
     * // 示例 3：错误用法
     * try {
     *     const result = randint(10, 5);
     * } catch (e) {
     *     console.error(e.message); // 输出：'min cannot be greater than max'
     * }
     */
    randint: (min, max) => {
        if (typeof min !== 'number' || typeof max !== 'number') {
            throw new Error('Both min and max must be numbers');
        }
        if (min > max) {
            throw new Error('min cannot be greater than max');
        }
        return Math.round(Math.random() * (max - min)) + min;
    },

    /**
    * 从给定的字符串中随机选择一个字符。
    * 如果输入为 `undefined`，会记录错误并返回错误信息。
    * @param {string | undefined} str - 要随机选择字符的字符串。如果未定义，将触发错误日志。
    * @returns {string} 返回从输入字符串中随机选择的单个字符，或者错误信息。
    *
    * @example
    * // 示例 1：正常输入
    * choice("hello"); // 可能返回 'h', 'e', 'l', 'l' 或 'o'
    *
    * @example
    * // 示例 2：输入为 undefined
    * choice(undefined); // 返回并记录 'IndexError'
    */
    choice: (str) => {
        if (str === undefined) {
            str = logger.error('IndexError')
            return str
        }
        str = str[Math.floor(Math.random() * str.length)]
        return str
    },

    /**
   * 从给定列表中随机选择多个元素。
   *
   * @param {Array} list - 要从中随机选择的数组。
   * @param {number} num - 要选择的元素数量。
   * @returns {Array} 返回包含随机选择的元素的新数组。
   *
   * @throws {TypeError} 如果 `list` 不是一个数组，或 `num` 不是一个有效数字。
   * @throws {RangeError} 如果 `num` 小于 0 或大于 `list` 的长度。
   *
   * @example
   * // 示例 1：从数组中随机选择 3 个元素
   * const items = ['apple', 'banana', 'cherry', 'date', 'elderberry'];
   * const result = sample(items, 3);
   * console.log(result); // 可能输出：['banana', 'date', 'apple']
   *
   * @example
   * // 示例 2：处理边界情况
   * const result = sample(['a', 'b', 'c'], 0);
   * console.log(result); // 输出：[]
   */
    sample: (list, num) => {
        if (!Array.isArray(list)) {
            throw new TypeError('Expected the first argument to be an array.');
        }
        
        if (typeof num !== 'number' || num < 0 || !Number.isInteger(num)) {
            throw new TypeError('Expected the second argument to be a non-negative integer.');
        }

        if (num > list.length) {
            throw new RangeError('The number of items to sample cannot exceed the size of the array.');
        }

        const new_list = [];
        for (let i = 0; i < num; i++) {
            new_list.push(_.sample(list));
        }
        return new_list;
    }
}

export default random;