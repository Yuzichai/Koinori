import { loadData } from './fishing/util.js'
import fs from 'fs'
import pt from 'path'
const __dirname = pt.resolve();

// 用于用户金钱控制
// get_user_money(user_id, key) return int    获取某种资源用户有多少
// set_user_money(user_id, key, value)        直接设置用户某种资源为多少
// increase_user_money(user_id, key, value)   增加用户某种资源多少
// reduce_user_money(user_id, key, value)     减少用户某种资源多少（含数值校验）
// increase_all_user_money(key, value)         增加全部用户某种资源多少
// translatename(name)                        将货币昵称转换成关键字
// tran_kira(uid, key, num)                   将羽毛石转换成某个其他物资（请先用translatename(name) 转换为关键字）


const path = __dirname + '\/plugins\/Koinori\/apps\/icelogin\/user_money.json'.replace(/\\/g, "/");
const bg_path = __dirname + '\/plugins\/Koinori\/apps\/icelogin\/user_background.json'.replace(/\\/g, "/");
const config = {  // 初始物资
    "default": {
        "gold": 200,  // 金币
        "luckygold": 0,  // 幸运币
        "starstone": 12500,  // 星星
        "kirastone": 0,  // 羽毛石
        "last_login": 0,  // 最后签到日期   防止单日多次签到
        "rp": 0,  // 记录运势rp值   只是防止单日多次抽签rp改变，不做其他用途
        "logindays": 0,  // 记录连续签到次数
        "exgacha": 0,
        "goodluck": 0,  // 宜做事项索引
        "badluck": 0,  // 忌做事项索引
    }
}

const keyword_list = [  // 避免错误设置
    "gold",
    "luckygold",
    "starstone",
    "kirastone",
    "last_login",
    "rp",
    "logindays",
    "exgacha",
    "goodluck",
    "badluck"
]

let user_money = {}

let key_list = ["gold", "luckygold", "starstone", "kirastone"]  // 钱包里的货币：金币，幸运币，星星

let name_list = {
    "starstone": ["starstone", "星星", "星石", "星",
        "stars", "爱星", "艾星"],
    "luckygold": ["luckygold", "lucky", "幸运",
        "幸运币"],
    "gold": ["gold", "金币", "金子", "黄金"
    ],
    "exgacha": ["井券", "兑换券", "exgacha"],
    "kirastone": ["羽毛石"]
}


function translatename(name) {
    for (var key in name_list) {
        if (name.includes(name_list[key])) {
            return key
        }
        else {
            return ''
        }
    }
}

function load_user_money() {
    try {
        if (!fs.existsSync(path)) {
            return 0;
        }
        let f = JSON.parse(fs.readFileSync(path, 'utf8'));
        for (var k in f) {
            for (var v in f[k]) {
                user_money[k] = v;
            }
        }
        fs.writeFileSync(path, JSON.stringify(user_money), 'utf8');
        return 1;
    } catch (error) {
        return 0;
    }
}

load_user_money()


function get_user_money(user_id, key) {
    /**
     * 自带初始化的读取钱包功能
     */

    load_user_money()
    if (!key.includes(keyword_list)) return null

    user_id = String(user_id)
    if (!user_id.includes(user_money)) {
        user_money[user_id] = {}
        for (var k in config['default'].items()) {
            for (var v in config['default'].items()) {
                user_money[user_id][k] = v
            }
        }
        let f = JSON.parse(fs.readFileSync(path, 'utf8'))
        fs.writeFileSync(JSON.stringify(user_money, f))
        if (key.includes(user_money[user_id])) {
            return user_money[user_id][key]
        }
        else {
            return null
        }
    }
}


function set_user_money(uid, key, value) {
    /**
     * 自带初始化的设置货币功能
     */
    if (!key.includes(keyword_list)) {
        uid = String(uid)
        if (user_money[uid] == undefined) {
            user_money[user_id] = {}
            for (var k in config['default']) {
                for (var v in config['default'][k]) {
                    user_money[user_id][k] = v
                }
            }
            user_money[user_id][key] = value
            let f = fs.readFileSync(path, 'utf8')
            fs.writeFileSync(JSON.stringify(user_money, f))
        }
    }
}


function increase_user_money(uid, key, value) {
    /**
     * 自带初始化的增加货币功能
     */
    if (uid == 80000000) {
        return
    }

    if (!key.includes(keyword_list)) {
        uid = String(uid)
        if (user_money[uid] == undefined) {
            user_money[user_id] = {}
            for (var k in config['default']) {
                for (var v in config['default'][k]) {
                    user_money[user_id][k] = v
                }
            }
            if (!key.includes(user_money[uid])) {
                user_money[user_id][key] = config['default'][key] + value
            }
            else {
                let now_money = get_user_money(uid, key) + value
                user_money[user_id][key] = now_money
            }
            let f = JSON.parse(fs.readFileSync(path, 'utf8'))
            fs.writeFileSync(JSON.stringify(user_money, f))
        }
    }
}

function reduce_user_money(uid, key, value) {
    /**
     * 自带初始化的减少货币功能
     */
    if (uid == 80000000) {
        return
    }

    if (!key.includes(keyword_list)) {
        uid = String(uid)
        if (user_money[uid] == undefined) {
            user_money[user_id] = {}
            for (var k in config['default']) {
                for (var v in config['default'][k]) {
                    user_money[user_id][k] = v
                }
            }
            if (!key.includes(user_money[uid])) {
                user_money[user_id][key] = config['default'][key] - value
            }
            else {
                let now_money = get_user_money(uid, key) - value
                if (now_money < 0) {
                    return 0
                }
                user_money[user_id][key] = now_money
            }
            let f = JSON.parse(fs.readFileSync(path, 'utf8'))
            fs.writeFileSync(JSON.stringify(user_money, f))
        }
    }
}


function tran_kira(uid, key, num) {
    if (key == 'gold') {
        value = num * 10
    }
    else if (key == 'starstone') {
        value = num * 10
    }
    else if (key == 'luckygold') {
        value = num // 50
        num = value * 50
    }
    else {
        value = 0
        num = 0
    }
    increase_user_money(uid, key, value)
    reduce_user_money(uid, 'kirastone', num)
    return num, value
}


function load_user_background() {
    if (!fs.existsSync(bg_path)) {
        let empty_dict = {}
        let f = JSON.parse(fs.readFileSync(bg_path, 'utf8'))
        fs.writeFileSync(JSON.stringify(empty_dict, f))
        return {}
    }
    else {
        let user_dict
        try {
            user_dict = loadData(bg_path)
        } catch (err) {
            logger.error('用户背景图片配置加载失败。')
            user_dict = {}
        }
        return user_dict
    }
}

var user_bg = load_user_background()


function get_user_background(uid) {
    if (uid == 80000000)
        return { 'default': '', 'custom': '', 'mode': 0 }
    user_bg = load_user_background()
    return user_bg[String(uid)]; //if (String(uid).includes(user_bg)) return { 'default': '', 'custom': '', 'mode': 0 }
}


function set_user_background(uid, bg, kind = 'default') {
    if (uid == 80000000) {
        return
    }
    uid = String(uid)
    if (user_bg[uid] == undefined) {
        user_bg[user_id] = { 'default': '', 'custom': '', 'mode': 0 }
        user_bg[user_id][kind] = bg
        let f = JSON.parse(fs.readFileSync(bg_path, 'utf8'))
        fs.writeFileSync(JSON.stringify(user_bg, f))
    }
}
function set_user_bg_mode(uid, mode) {
    /**
     * :param: mode:0-默认，1-hoshi，2-自定义
     */
    if (uid == 80000000) {
        return
    }

    uid = String(uid)
    if (user_bg[uid] == undefined) {
        user_bg[user_id] = { 'default': '', 'custom': '', 'mode': 0 }
        user_bg[user_id]['mode'] = mode

        let f = JSON.parse(fs.readFileSync(bg_path, 'utf8'))
        fs.writeFileSync(JSON.stringify(user_bg, f))
    }
}


function check_mode(uid) {
    if (user_bg[uid] == undefined) {
        set_user_bg_mode(uid, 0)
        return
    }
    if (user_bg[String(uid)]['custom']) {
        set_user_bg_mode(uid, 2)
    }
    else if ('hoshi'.includes(user_bg[str(uid)]['default'])) {
        set_user_bg_mode(uid, 1)
    }
    else if (user_bg[String(uid)]['default']) {
        set_user_bg_mode(uid, 0)
    }
    else {
        set_user_background(uid, 'Background3.jpg')
        set_user_bg_mode(uid, 0)
    }
}


export default { translatename, load_user_money, get_user_money, set_user_money, increase_user_money, reduce_user_money, tran_kira, load_user_background, get_user_background, set_user_bg_mode, check_mode }