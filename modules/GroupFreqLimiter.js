import moment from 'moment'

let currentTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')

function set_reload_group(gid, _time) {
    redis.set(`reload_group${gid}`, currentTime, {
        EX: _time
    })
}


function check_reload_group(gid, _type) {
    let remain = redis.get(`reload_group${gid}`)
    if (_type == 'boolean') {
        return false
    }
    else {
        remain = moment(currentTime).diff(moment(remain), 'remain')
        return remain
    }
}

export { set_reload_group, check_reload_group }