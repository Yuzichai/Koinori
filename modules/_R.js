import fs from 'fs'

const res_dir = process.cwd() + 'plugins/modules/src/'

function R(obj, self) {
    if (obj == 'img') {
        let imgPath = `file:///` + res_dir.concat('img/') + self
        return imgPath
    }
    else if (obj == 'db') {
        let userPath = fs.readFileSync(res_dir + 'database/' + self, 'utf8')
        return userPath
    }
    else {
        return null
    }
}


export default { R }
