import fs from 'fs'

/** 
 * 保存数据
 * @param obj: 将要保存的数据
 * @param fp: 文件路径
 * @returns
 */
function saveData(obj, fp) {
    let file = fs.readFileSync(fp, 'utf8')
    fs.writeFileSync(obj, JSON.stringify(file))
}

/**
 * 加载json，不存在则创建
 * @param  path: 文件路径
 * @returns 
 */
function loadData(path) {
    let fp = JSON.parse(fs.readFileSync(path, 'utf8'))
    if (fp === undefined) fp = fs.writeFileSync(path, '{}')
    return fp
}


function check_user(uid, _dict) {
    if (_dict[uid] === undefined) {
        _dict[uid] = { "switch": 1, "other": '', "self": '' }
        return _dict
    }
}

async function get_user_icon(uid) {
    let imageUrl = `https://q1.qlogo.cn/g?b=qq&nk=${uid}&src_uin=www.jlwz.cn&s=0`
    return imageUrl
}

export { saveData, loadData, check_user, get_user_icon }