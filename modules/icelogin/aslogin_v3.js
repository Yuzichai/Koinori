import money from '../money.js'
import fetch from 'node-fetch'

let debug_mode = 0  // 调试模式
let add_watermark = 0  // 调试模式是否添加水印
let debug_login_flag = 0  // 处于调试模式时签到标志
let save_image_mode = 0  // 将签到卡片保存到本地
let debug_background = 'Background2.jpg'  // 用于调试模式的图片

let random_string = 'ę‘†č„±ę‰€ę‰č¨·å…°ē„å¤§å˛‹å¤©äø»ę•™č‰ŗęÆē„ę¯ē¼ļ¼č¨·å…°äŗŗåÆ¹č‰ŗęÆē„é‰´čµ¸å›å¾—ä»�ę¸é«ć€‚é¸ē¯€č´øę“ē„å¸‘å±•å’ē»¸ęµˇē„ē¹č¨£ļ¼č¨·å…°čæˇę¯�äŗ†č‡Ŗå·±ē„ā€é»„é‡‘å¹´ä»£ā€¯ļ¼åę—¶å¼•čµ·äŗ†ę–‡å–ę„¸čÆ†ē„č§‰é†’å’č‡Ŗäæ�ē„é«ę¶Øć€‚ē„¶č€ļ¼å·´ę´›å…‹é£ˇę ¼ē„ē¹č¨£å’å¤©äø»ę•™ę•™ä¹‰ē„ååØļ¼äøˇč‡Ŗę‘å¦å®å’č‚å¶ē„ę–°ę•™ē†č®ŗęŖē„¶äø¨åć€‚é™¤äŗ†å¸—å§”ę‰å›ä½č‚–å¸ē”»ä»�å¤–ļ¼č‰ŗęÆå®¶ä»¬å¹¶ę²�ę‰ēę£ē„ē›®ē„ļ¼ä»–ä»¬ē„äø“äøå°ä½¨ä¹å²å²å¸Æå¨±ć€‚'

let backgroundList = ['Background1.jpg', 'Background2.jpg', 'Background3.jpg', 'Background4.jpg', 'Background5.jpg',
    'Background6.jpg', 'Background7.jpg', 'Background8.jpg', 'Background9.jpg', 'Background10.jpg',
    'Background11.jpg', 'Background12.jpg', 'Background13.jpg', 'Background14.jpg']

let hoshi_bg_list = ['Background-hoshi-1.jpg', 'Background-hoshi-2.jpg', 'Background-hoshi-3.jpg', 'Background-hoshi-4.jpg',
    'Background-hoshi-5.jpg']

let extra_bg_list = ['Background_extra.jpg']

let goodluck = ['宜 抽卡', '宜 干饭', '宜 摸鱼', '宜 刷副本',
    '宜 女装', '宜 打游戏', '宜 刷b站', '宜 看涩图',
    '宜 逛街', '宜 好好学习', '宜 搓麻将', '宜 工作',
    '宜 点外卖', '宜 水群', '宜 听音乐', '宜 背单词',
    '宜 做作业', '宜 刷抖音', '宜 睡觉', '宜 刷剧'
]

let badluck = ['忌 抽卡上头', '忌 躺平', '忌 摸鱼', '忌 刷副本',
    '忌 女装', '忌 打游戏', '忌 刷b站', '忌 看涩图',
    '忌 逛街', '忌 学习', '忌 搓麻将', '忌 摆烂',
    '忌 点外卖', '忌 水群', '忌 听音乐', '忌 背单词',
    '忌 做作业', '忌 刷抖音', '忌 睡懒觉', '忌 刷剧', '忌 无'
]

let birth_list = ["1232"
]

let member_list = ["冰祈"
]

let event_list = ['0101', '0122', '0205', '0214', '0308', '0401', '0405', '0501',
    '0601', '0622', '0701', '0801', '0822', '0929', '1001',
    '1010', '1011', '1101', '1111', '1224', '1225'
]

let event_name_list = ['元旦节', '春节', '元宵节', '情人节', '妇女节', '愚人节', '清明节', '劳动节',
    '儿童节', '端午节', '建党节', '建军节', '七夕节', '中秋节', '国庆节',
    '萌节', '萝莉节', '万圣节', '光棍节', '平安夜', '圣诞节'
]

let week_list = ["日", "一", "二", "三", "四", "五", "六"]

let srcpath = process.cwd() + '/plugins/Icepray/apps/icelogin/src'

async function creep_img(session, url, uid) {
    let imgname = `${uid}.jpg`
    let res = await fetch(url).then(r => {
        if (r.ok) { return true } else { return false }
    }).catch(() => false)


    return imgname
}


function check_str_len(string) {
    let lenTxt = string.length
    let size = lenTxt
    return size
}

function get_day(day) { // ===日期===
    let months = day.getMonth()
    let days = day.getDate()
    switch (day) {
        case 0:
            day = "星期日"
            break
        case 1:
            day = "星期一"
            break
        case 2:
            day = "星期二"
            break
        case 3:
            day = "星期三"
            break
        case 4:
            day = "星期四"
            break
        case 5:
            day = "星期五"
            break
        case 6:
            day = "星期六"
            break
    }
    let time = (months + 1) + "月" + days + "日" + day
    return time
}


function luck_choice(which) { // 获取宜忌索引
    let good_choice = goodluck[Math.floor(Math.random() * goodluck.length)]
    let bad_choice = badluck[Math.floor(Math.random() * badluck.length)]
    if (which == 0) {
        return good_choice
    }
    if (which == 1) {
        return bad_choice
    }
}


function feed_back(value) {
    let info = ''
    if (value == 0) {
        info = 'QAQ...冰祈不是故意的...'
    } else if (value == 101) {
        value = 999
        info = '999！是隐藏的999运势！'
    } else if (value == 100) {
        info = '100！今天说不定能发大财！！！'
    } else if (value >= 20 && value < 40) {
        info = '运势欠佳哦，一定会好起来的！'
    } else if (value >= 40 && value < 60) {
        info = '运势普普通通，不好也不坏噢~'
    } else if (value >= 60 && value < 80) {
        info = '运势不错~会有什么好事发生吗？'
    } else if (value >= 80 && value < 90) {
        info = '运势旺盛！今天是个好日子~'
    } else if (value >= 90 && value <= 99) {
        info = '好运爆棚！一定会有好事发生吧！'
    } else {
        info = '运势很差呢，摸摸...'
    }
}


async function as_login_v3(uid, username, qqname, nick_flag) {
    // === 初始化 ===
    let festival_msg = ''  // 节日提示
    let birthday_msg = ''  // 生日提示
    let login_flag_msg = ''  // 已经签到的提示
    let date_msg = ''  // 日期提示
    let name_msg = ''  // 姓名提示
    let good_luck_msg = ''  // 运势提示 宜
    let bad_luck_msg = ''  // 运势提示 忌
    let extra_msg = ''  // 首次签到的提示
    let rp_colormap = (0, 0, 0)  // 人品色板
    let luckygold_msg = ''  // 总收益中幸运币提示
    let total_get_msg = ''  // 今日签到总收益提示

    let last_login = money.get_user_money(uid, "last_login")  // 是否最近签到

    let gold = 50  // 签到增加的基础金币

    let index = event_list.indexOf(moment(new Date()).format("MMDD"))
    if (!index == -1) return event_name_list[index]
    let event_flag = event_name_list[index]

    if (!login_flag) {
        money.increase_user_money(uid, "logindays", 1)  // 签到天数
    }
    else {
        let h = money.get_user_money(uid, "rp")
        let _h = h
        let rp = Math.floor(Math.radmom() * 101)

        let info = feed_back(rp) // 人品吐槽

        // === 获取今日宜忌 ===

        let good_todo_index, bad_todo_index
        if (login_flag) {
            good_todo_index = money.get_user_money(uid, "goodluck")
        }
        else {
            good_todo_index = luck_choice(0)
        }

        if (login_flag) {
            bad_todo_index = money.get_user_money(uid, "badluck")
        }
        else {
            bad_todo_index = luck_choice(1)
        }

        let str1 = good_todo_index.slice(1)
        let str2 = bad_todo_index.slice(1)
        if (str1 == str2) luck_choice(1) // 避免相等


        // === 幸运币 ===
        let luckygold_num;
        if (90 <= rp <= 91 && !login_flag) {
            luckygold_num = 1;
            money.increase_user_money(uid, "luckygold", luckygold_num)
        }
        else if (rp == 100 && !login_flag) {
            luckygold_num = 10
            money.increase_user_money(uid, "luckygold", luckygold_num)
        }
        else if (rp == 999) {
            luckygold_num = 20
            money.increase_user_money(uid, "luckygold", luckygold_num)
        }

        gold += rp
        let logindays = money.get_user_money(uid, "logindays")
        //累签星星数
        let star_add = min(500, (logindays / 10) * 50)
        //累签金币数
        let gold_add = min(100, max(0, (logindays / 10) * 5))

        if (uid == 80000000) {
            rp = -1
            info = '  Vanitas vanitatum,Et omnia vanitas.'
            good_luck_msg = '宜 取消匿名'
            bad_luck_msg = '忌 匿名'
            extra_msg = ''
            login_flag = 0
        }

        // ------> 下面开始绘图 <------
        money.check_mode(uid)
        let user_bg = money.get_user_background(uid)
        // === 头像部分 ===
        let imageUrl = `https://q1.qlogo.cn/g?b=qq&nk=${uid}&src_uin=www.jlwz.cn&s=0`

        // === 日期+累计签到部分 ===
        let day = new Date()
        let time = get_day(day)
        time += `     累计签到${days}天`

        // === 用户名部分 ===
        let size = check_str_len(qqname)
        let final_txt = ''
        let txt_size
        if (size >= 20) {
            for (var i in qqname) {
                qqname += i
                txt_size = check_str_len(final_txt)
                if (txt_size >= 20) break
            }
        }
        else {
            final_txt = qqname

        }

        let event_gold = 200;
        let event_star = 400;
        let Data = {
            tplFile: './plugins/Icepray/res/qiandao/qiandao.html',
            cssFile: `./plugins/Icepray/res/qiandao`,
            saveId: 'icelogin',
            imgType: 'png',
            img: user_bg,
            tou: imageUrl,
            name: final_txt,
            what: `${good_luck_msg}  ${bad_luck_msg}`,
            info: info,
            rp: rp,
            luckygold: luckygold_num,
            gold_add: gold_add,
            star_add: star_add,
            event_gold: event_gold,
            event_star: event_star,
            bsgold: bsgold,
            bsstar: bsstar,
            evgold: evgold,
            evstar: evstar,
            index: index,
            event_flag: event_flag,
            islogin: login_flag,
            uid: uid,
            time: time,
            day: days,
            r: r,
            g: g,
            b: b
        }
        let imageToSend = await puppeteer.screenshot(`${uid}-${login_flag}`, Data)
        logger.info('绘制签到图片完成')
        return imageToSend
    }
}


function min(x, y) {
    let z = Math.min(x, y)
    return z
}

function max(x, y) {
    let z = Math.max(x, y)
    return z
}